**NOTE: KEEP IN MIND THIS IMAGE IS NO LONGER SUPPORTED, AND SO DOES SCIENTIFIC LINUX 5**

# Scientific Linux CERN 5 Docker images

http://linux-old.web.cern.ch/linux-old/scientific5/

## What is Scientific Linux CERN 5 (SLC5)

Scientific Linux CERN 5 is a Linux distribution build within the framework of Scientific Linux which in turn is rebuilt from the freely available Red Hat Enterprise Linux 5 (Server) product sources under terms and conditions of the Red Hat EULA. Scientific Linux CERN is built to integrate into the CERN computing environment but it is not a site-specific product: all CERN site customizations are optional and can be deactivated for external users.

## Current release: SLC 5.11

Scientific Linux CERN 5.11 is the last minor release of SLC5

### Image building

```
koji image-build slc5-base 5.x.`date "+%Y%m%d"` slc5-image-5x http://linuxsoft.cern.ch/cern/slc5X/x86_64 x86_64 \
    --ksurl=git+ssh://git@gitlab.cern.ch:7999/linuxsupport/slc5-base#master \
    --kickstart=slc5-base-docker.ks --distro RHEL-5.11 --format docker --ksversion RHEL5 --factory-parameter dockerversion 1.10.1 \
    --factory-parameter=docker_env '["PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"]' \
    --factory-parameter=docker_cmd '["/bin/bash"]' \
    --scratch
```

note: ksversion RHEL5 <-- otherwise kickstart sections not ending in %end will fail.

### Latest image

```20190724: https://kojitest.cern.ch/taskinfo?taskID=1005```

```20180316: http://koji.cern.ch/koji/taskinfo?taskID=1075039```

```20180112: http://koji.cern.ch/koji/taskinfo?taskID=1020942```

```20171114: http://koji.cern.ch/koji/taskinfo?taskID=973571```

```20170920: http://koji.cern.ch/koji/taskinfo?taskID=928007```
