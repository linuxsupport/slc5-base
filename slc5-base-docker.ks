#
# CERN SLC5X, based on CentOS Cloud SIG , building on tag slc5-image-5x
#
install
reboot
text
keyboard 'us'
lang en_US.UTF-8
network --bootproto dhcp --device eth0 --noipv6 eth0
rootpw --iscrypted $NOT-A-ROOT-PASSWORD$
timezone --utc Europe/Zurich
authconfig --enableshadow --enablemd5
firewall --disabled
# would need centosplus for 5
selinux --disabled

url  --url="http://linuxsoft.cern.ch/cern/slc5X/x86_64/"

# Dec 2015 - we have a problem here: anaconda in 5.11 tries to load rpms from wrong URLs ..?
# so we just use url above that koji doe s not overwrite and do the stuff in postinstall

repo --name="SLC5X" --baseurl="http://linuxsoft.cern.ch/cern/slc5X/x86_64/"
repo --name="SLC5X updates" --baseurl="http://linuxsoft.cern.ch/cern/slc5X/x86_64/yum/updates/"
repo --name="SLC5X extras" --baseurl="http://linuxsoft.cern.ch/cern/slc5X/x86_64/yum/extras/"

zerombr
clearpart --all --initlabel
part / --fstype ext3 --size=1024 --grow


#
# since we do use the workaround above ... ONLY what is in os repo is available in % packages,
# for all the rest use % post ...
# jarek 01.12.2015
#


%packages  --nobase
bash
bind-utils
CERN-CA-certs
cern-wrappers
cyrus-sasl-gssapi
-*-firmware
grub
hepix
-kernel
krb5-workstation
openldap-clients
-perl
shadow-utils
sl-release
vim-minimal
yum
yum-autoupdate
-yum-firstboot

%post

rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-cern
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL

yum -y update

#yum -y install cern-get-sso-cookie

# randomize root password and lock root account
dd if=/dev/urandom count=50 | md5sum | passwd --stdin root
passwd -l root

# create necessary devices
/sbin/MAKEDEV /dev/console

# cleanup unwanted stuff
rm -rf /boot

# some packages get installed even though we ask for them not to be,
# and they don't have any external dependencies that should make
# anaconda install them

yum -y remove openldaplibuser iscsi-initiator-utils kernel passwd mkinitrd \
       policycoreutils system-config-securitylevel-tui \
       libhugetlbfs redhat-menus Deployment_Guide\* \
       audit-libs-python checkpolicy libvolume_id \
       libselinux-python libselinux-utils wireless-tools rhpl udftools \
       desktop-file-utils pciutils sysfsutils \
       cyrus-sasl-lib openldap libuser cryptsetup-luks libgcrypt \
       prelink hdparm newt ed sgpio dmraid-events dmraid libgcc.i386 \
       libtermcap.i386 readline.i386 libsemanage redhat-logos grub perl | tee /root/yum-removal.log

# Can't remove docs on c5 due to a bug similar to
# https://bugzilla.redhat.com/show_bug.cgi?id=515911
# Keep yum from installing documentation. It takes up too much space.
#sed -i '/distroverpkg=centos-release/a tsflags=nodocs' /etc/yum.conf

#Generate installtime file record
/bin/date +%Y%m%d_%H%M > /etc/BUILDTIME



# nuking the locales breaks things. Lets not do that anymore strip
# most of the languages from the archive. stolen from
# https://bugzilla.redhat.com/show_bug.cgi?id=156477#c28
localedef --delete-from-archive $(localedef --list-archive | \
    grep -v -i ^en | xargs )
# prep the archive template
mv /usr/lib/locale/locale-archive  /usr/lib/locale/locale-archive.tmpl
# rebuild archive
/usr/sbin/build-locale-archive
#empty the template
:>/usr/lib/locale/locale-archive.tmpl

# but that is ... 150 MB used, so lets try again:
rm -rf $(find /usr/lib/locale/ | grep '_' | grep -vi '/en' | xargs )

#  man pages and documentation
find /usr/share/{man,doc,info,gnome/help} -type f -delete

#  sln
rm -f /sbin/sln

#  ldconfig
rm -rf /etc/ld.so.cache
#rm -rf /var/cache/ldconfig/

# Clean up after yum
rm -rf /var/lib/yum/*
rm -rf /var/cache/yum/*

# Clean up after the installer.
rm -f /etc/rpm/macros.imgcreate

# and selinux (uninstalled anyway)
rm -rf /etc/selinux

rm -f /var/log/anaconda.syslog
rm -f /var/log/anaconda.log

############# CERN'ify ########################################################


cat > /etc/krb5.conf <<EOF
[libdefaults]
 default_realm = CERN.CH
 ticket_lifetime = 25h
 renew_lifetime = 120h
 forwardable = true
 proxiable = true
 default_tkt_enctypes = arcfour-hmac-md5 aes256-cts aes128-cts des3-cbc-sha1 des-cbc-md5 des-cbc-crc
 allow_weak_crypto = true
 chpw_prompt = true

[realms]
 CERN.CH = {
  default_domain = cern.ch
  kpasswd_server = cerndc.cern.ch
  admin_server = cerndc.cern.ch
  kdc = cerndc.cern.ch
  }

[domain_realm]
 .cern.ch = CERN.CH

pam = {
   external = true
   krb4_convert =  false
   krb4_convert_524 =  false
   krb4_use_as_req =  false
   ticket_lifetime = 25h
   use_shmem = sshd
 }

EOF

cat > /etc/openldap/ldap.conf <<EOF
#
# LDAP CERN Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

#BASE DC=cern,DC=ch
#note cerndc provides gssapi auth, xldap does not.
#HOST cerndc.cern.ch  # or xldap.cern.ch
#SIZELIMIT 12
#DEREF always

TLS_CACERTDIR /etc/openldap/certs
TLS_REQCERT demand
SSL start_tls

# Turning this off breaks GSSAPI used with krb5 when rdns = false
SASL_NOCANON	on

EOF
